//
//  CSViewController.h
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-12-20.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSCardMatchingGame.h"
#import "CSPlayingCardDeck.h"
#import "CSGameSettings.h"
#import "CSPlayingCard.h"
#import "CSGameResults.h"

@interface CSViewController : UIViewController
- (void)updateUI;
@end
