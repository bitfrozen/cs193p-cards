//
//  CSAppDelegate.h
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-12-20.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
