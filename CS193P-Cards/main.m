//
//  main.m
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-12-20.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CSAppDelegate class]));
    }
}
