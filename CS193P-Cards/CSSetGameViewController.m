//
//  CSSetGameViewController.m
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-12-29.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import "CSSetGameViewController.h"
#import "CSSetCardDeck.h"
#import "CSSetCard.h"

@interface CSSetGameViewController ()

@property(strong, nonatomic) CSCardMatchingGame *game;
@property(strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;
@property(strong, nonatomic) NSArray *cardSymbols;
@property(strong, nonatomic) NSArray *cardColors;

@end

@implementation CSSetGameViewController

@synthesize game = _game;
@synthesize cardButtons = _cardButtons;
@synthesize cardSymbols = _cardSymbols;
@synthesize cardColors = _cardColors;

- (id)parseGameMove:(CSGameMove *)move
{
    NSMutableAttributedString *moveDescription;
    BOOL addAnd = NO;
    switch (move.moveType) {
        case MoveTypeFlip:
            moveDescription = [[NSMutableAttributedString alloc] initWithString:@"Flipped up "];
            [moveDescription appendAttributedString:[self parseAttributesForCard:[move.cardsInMove lastObject]]];
            break;
        case MoveTypeMatch:
            moveDescription = [[NSMutableAttributedString alloc] initWithString:@"Matched "];
            for (CSSetCard *card in move.cardsInMove) {
                if (addAnd) {
                    [moveDescription appendAttributedString:[[NSAttributedString alloc] initWithString:@" & "]];
                } else {
                    addAnd = YES;
                }
                [moveDescription appendAttributedString:[self parseAttributesForCard:card]];
            }
            [moveDescription appendAttributedString:
                                 [[NSAttributedString alloc]
                                     initWithString:[[NSString alloc] initWithFormat:@" for %ld points", (long)move.moveScore]]];
            break;
        case MoveTypeMismatch:
            moveDescription = [[NSMutableAttributedString alloc] initWithString:@""];
            for (CSSetCard *card in move.cardsInMove) {
                if (addAnd) {
                    [moveDescription appendAttributedString:[[NSAttributedString alloc] initWithString:@" & "]];
                } else {
                    addAnd = YES;
                }
                [moveDescription appendAttributedString:[self parseAttributesForCard:card]];
            }
            [moveDescription
                appendAttributedString:
                    [[NSAttributedString alloc]
                        initWithString:[[NSString alloc] initWithFormat:@" don't match. %ld point penalty!", (long)move.moveScore]]];
            break;

        default:
            break;
    }
    DLog(@"%@", moveDescription);
    return moveDescription;
}

- (void)renderCards
{
    for (UIButton *cardButton in self.cardButtons) {
        CSCard *card = [self.game cardAtIndex:[self.cardButtons indexOfObject:cardButton]];
        [cardButton setAttributedTitle:[self parseAttributesForCard:(CSSetCard *)card] forState:UIControlStateNormal];
        [cardButton setBackgroundColor:card.isFaceUp ? [UIColor colorWithWhite:0.9f alpha:1.0f] : [UIColor whiteColor]];
        cardButton.selected = [card isFaceUp];
        cardButton.enabled = ![card isUnplayable];
        cardButton.alpha = ([card isUnplayable] ? 0.0f : 1.0f);
    }
}

- (NSAttributedString *)parseAttributesForCard:(CSSetCard *)card
{
    NSMutableString *symbol = [[NSMutableString alloc] initWithString:(self.cardSymbols)[card.shape]];
    for (NSUInteger i = 0; i < card.number; ++i) {
        [symbol appendString:(self.cardSymbols)[card.shape]];
    }
    UIColor *shapeColor
        = [(self.cardColors)[card.color] colorWithAlphaComponent:card.shade < 2 ? card.shade * 0.25f : 1.0f];
    UIColor *strokeColor = (self.cardColors)[card.color];
    return [[NSMutableAttributedString alloc] initWithString:symbol attributes:@{NSForegroundColorAttributeName:shapeColor, NSStrokeColorAttributeName:strokeColor, NSStrokeWidthAttributeName:@(-3.0)}];
}

#pragma mark -
#pragma mark Setters / Getters

- (NSArray *)cardSymbols
{
    if (!_cardSymbols)
        _cardSymbols = @[ @"▲", @"●", @"■" ];
    return _cardSymbols;
}

- (NSArray *)cardColors
{
    if (!_cardColors)
        _cardColors = @[ [UIColor redColor], [UIColor greenColor], [UIColor purpleColor] ];
    return _cardColors;
}

- (CSCardMatchingGame *)game
{
    if (!_game) {
        _game = [[CSCardMatchingGame alloc] initWithCardCount:[self.cardButtons count]
                                                    usingDeck:[[CSSetCardDeck alloc] init]
                                                     gameType:GameTypeSetMatchingGame
                                                   matchBonus:[CSGameSettings getSetPointsForRightSet]
                                                 mismatchCost:[CSGameSettings getSetPointsForWrongSet]
                                                     flipCost:0];
    }

    return _game;
}

- (void)setCardButtons:(NSArray *)cardButtons
{
    _cardButtons = cardButtons;
    [super updateUI];
}

@end
