//
//  CSGameResultsViewController.m
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-09-18.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import "CSGameResultsViewController.h"
#import "CSGameResults.h"

@interface CSGameResultsViewController ()
@property(weak, nonatomic) IBOutlet UITextView *scoreDisplay;

@end

@implementation CSGameResultsViewController

@synthesize scoreDisplay = _scoreDisplay;

- (IBAction)sortByDate
{
    [self updateUI:@selector(compareDates:)];
}

- (IBAction)sortByScore
{
    [self updateUI:@selector(compareScores:)];
}

- (IBAction)softByDuration
{
    [self updateUI:@selector(compareDuration:)];
}

- (void)updateUI:(SEL)selector
{
    UITextView *stScoreDisplay = self.scoreDisplay;
    NSString *displayText = @"";
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    // write header for scores screen
    displayText
        = [displayText stringByAppendingFormat:@"%5s   %6s %9s        %6s\n", [@"GAME" UTF8String],
                                               [@"SCORE" UTF8String], [@"DATE" UTF8String], [@"LENGTH" UTF8String]];
    displayText = [displayText stringByAppendingString:@"**************************************\n"];
    for (CSGameResults *result in [[CSGameResults allGameResults] sortedArrayUsingSelector:selector]) {
        displayText
            = [displayText stringByAppendingFormat:
                               @"%7s %4d   %16s %4gs\n", [[CSGameResults gameNameForType:result.gameType] UTF8String],
                               result.score, [[formatter stringFromDate:result.end] UTF8String], round(result.duration)];
    }
    // Fix for bug where UITextView will reset font settings if it's property 'selectable'
    // is FALSE during setText call
    stScoreDisplay.selectable = YES;
    stScoreDisplay.text = displayText;
    stScoreDisplay.selectable = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateUI:@selector(compareScores:)];
}

- (void)setup
{
    // initialization that can't wait until viewDidLoad
}

- (void)awakeFromNib
{
    [self setup];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

    [self setup];
    return self;
}

@end
