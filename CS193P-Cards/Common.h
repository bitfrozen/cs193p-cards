//
//  Common.h
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-12-29.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#ifndef COMMON_h
#define COMMON_h

// DLog will output like NSLog only when the DEBUG variable is set
#ifdef DEBUG
#define DLog(fmt, ...) NSLog((@"%s " fmt), __PRETTY_FUNCTION__, ##__VA_ARGS__);
#else
#define DLog(...)
#endif

// ALog will always output like NSLog
#define ALog(fmt, ...) NSLog((@"%s " fmt), __PRETTY_FUNCTION__, ##__VA_ARGS__);

// ULog will show the UIAlertView only when the DEBUG variable is set
#ifdef DEBUG
#define ULog(fmt, ...)                                                                                                 \
    {                                                                                                                  \
        UIAlertView *alert                                                                                             \
            = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%s\n ", __PRETTY_FUNCTION__]             \
                                         message:[NSString stringWithFormat:fmt, ##__VA_ARGS__]                        \
                                        delegate:nil                                                                   \
                               cancelButtonTitle:@"Ok"                                                                 \
                               otherButtonTitles:nil];                                                                 \
        [alert show];                                                                                                  \
    }
#else
#define ULog(...)
#endif

#endif
