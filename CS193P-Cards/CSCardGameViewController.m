//
//  CSCardGameViewController.m
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-12-29.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import "CSCardGameViewController.h"

@interface CSCardGameViewController ()

@property(strong, nonatomic) CSCardMatchingGame *game;
@property(strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;

@end

@implementation CSCardGameViewController

@synthesize game = _game;
@synthesize cardButtons = _cardButtons;

- (id)parseGameMove:(CSGameMove *)move
{
    NSString *moveDescription;
    switch (move.moveType) {
        case MoveTypeFlip:
            moveDescription = [NSString stringWithFormat:@"Flipped up %@", [move.cardsInMove lastObject]];
            break;
        case MoveTypeMatch:
            moveDescription
                = [NSString stringWithFormat:@"Matched %@ for %ld points",
                                             [move.cardsInMove componentsJoinedByString:@" & "], (long)move.moveScore];
            break;
        case MoveTypeMismatch:
            moveDescription
                = [NSString stringWithFormat:@"%@ don't match. %ld point penalty!",
                                             [move.cardsInMove componentsJoinedByString:@" & "], (long)move.moveScore];
            break;

        default:
            break;
    }
    DLog(@"%@", moveDescription);
    return [NSString stringWithFormat:@"%@", moveDescription];
}

- (void)renderCards
{
    UIImage *cardBack = [UIImage imageNamed:@"CardBackOrange"];
    UIImage *transparent = [UIImage imageNamed:@"TransparentImage"];

    for (UIButton *cardButton in self.cardButtons) {
        CSCard *card = [self.game cardAtIndex:[self.cardButtons indexOfObject:cardButton]];
        [cardButton setTitle:card.contents forState:UIControlStateSelected];
        [cardButton setTitle:card.contents forState:UIControlStateSelected | UIControlStateDisabled];
        [cardButton setImage:cardBack forState:UIControlStateNormal];
        [cardButton setImage:transparent forState:UIControlStateSelected];
        [cardButton setImage:transparent forState:UIControlStateSelected | UIControlStateDisabled];
        cardButton.selected = card.isFaceUp;
        cardButton.enabled = !card.isUnplayable;
        cardButton.alpha = card.isUnplayable ? 0.3 : 1.0;
    }
}

- (CSCardMatchingGame *)game
{
    if (!_game) {
        CSGameType gameType = ([CSGameSettings getCardGameType] == 0) ? GameTypeTwoCardMatchingGame
                                                                      : GameTypeThreeCardMatchingGame;
        _game = [[CSCardMatchingGame alloc] initWithCardCount:[self.cardButtons count]
                                                    usingDeck:[[CSPlayingCardDeck alloc] init]
                                                     gameType:gameType
                                                   matchBonus:[CSGameSettings getMatchismoMatchingPoints]
                                                 mismatchCost:[CSGameSettings getMatchismoMismatchPoints]
                                                     flipCost:[CSGameSettings getMatchismoFlipPoints]];
    }

    return _game;
}

- (void)setCardButtons:(NSArray *)cardButtons
{
    _cardButtons = cardButtons;
    [super updateUI];
}

@end
