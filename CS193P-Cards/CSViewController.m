//
//  CSViewController.m
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-12-20.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import "CSViewController.h"

#pragma mark -
#pragma mark Class Extensions

@interface CSViewController ()
@property(strong, nonatomic) CSCardMatchingGame *game;
@property(strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;
@property(weak, nonatomic) IBOutlet UILabel *flipsLabel;
@property(weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property(nonatomic) NSUInteger flipsCount;
@property(weak, nonatomic) IBOutlet UILabel *gameHistoryLabel;
@property(weak, nonatomic) IBOutlet UISlider *gameHistorySlider;
@property(strong, nonatomic) NSMutableArray *gameHistoryCollection; // NSString
@property(strong, nonatomic) CSGameResults *gameResult;

- (IBAction)flipCard:(UIButton *)sender;
- (IBAction)startGame:(UIButton *)sender;
- (IBAction)moveHistorySlider:(UISlider *)sender;

@end

#pragma mark -
#pragma mark Class Implementation

@implementation CSViewController

@synthesize flipsLabel = _flipsLabel;
@synthesize scoreLabel = _scoreLabel;
@synthesize flipsCount = _flipsCount;
@synthesize cardButtons = _cardButtons;
@synthesize game = _game;
@synthesize gameHistoryLabel = _gameHistoryLabel;
@synthesize gameHistorySlider = _gameHistorySlider;
@synthesize gameHistoryCollection = _gameHistoryCollection;
@synthesize gameResult = _gameResult;

/**
 *  Parsing game move. Passing move description from game model. Parsing it and
 *  returning NSString description of the move.
 *
 *  @param move Array collection of game cards included in move.
 *
 *  @return NSString description of the move
 */
- (id)parseGameMove:(CSGameMove *)move
{
    @throw [NSException
        exceptionWithName:NSInternalInconsistencyException
                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                 userInfo:nil];
    return nil;
}

- (IBAction)flipCard:(UIButton *)sender
{
    if ([self.game flipCardAtIndex:[self.cardButtons indexOfObject:sender]]) {
        // add move description only if there was a move (change inside model)
        [self.gameHistoryCollection addObject:[self parseGameMove:[self.game lastGameMove]]];
        self.gameHistorySlider.maximumValue = [self.gameHistoryCollection count];
        self.gameHistorySlider.value = self.gameHistorySlider.maximumValue;
        [self moveHistorySlider:nil];
        self.gameResult.score = self.game.score;
    }
    [self updateUI];
    ++self.flipsCount;
    // check game state after all flip stages are done
    if (![self.game areAnyMatchesAvailable]) {
        [self showGameEndAlert];
    }
}

- (IBAction)startGame:(UIButton *)sender
{
    [self resetGame];
}

- (IBAction)moveHistorySlider:(UISlider *)sender
{
    NSUInteger indexToLook = (NSUInteger)ceilf(self.gameHistorySlider.value) - 1;
    NSUInteger historyCount = [self.gameHistoryCollection count];
    if (indexToLook < historyCount) {
        NSMutableAttributedString *currentLabel;
        if ([self.gameHistoryCollection[indexToLook] isKindOfClass:[NSAttributedString class]]) {
            currentLabel = self.gameHistoryCollection[indexToLook];
        } else {
            currentLabel = [[NSMutableAttributedString alloc] initWithString:self.gameHistoryCollection[indexToLook]];
        }
        // Assign center alignment before setting text
        NSMutableParagraphStyle *centerParagraphStyle = [[NSMutableParagraphStyle alloc] init];
        centerParagraphStyle.alignment = NSTextAlignmentCenter;
        [currentLabel addAttributes:@{
                                        NSParagraphStyleAttributeName : centerParagraphStyle,
                                        NSFontAttributeName : [UIFont systemFontOfSize:14.0f]
                                    }
                              range:NSMakeRange(0, [currentLabel length])];
        self.gameHistoryLabel.attributedText = currentLabel;
    }
    // change history label transparency
    if (indexToLook + 1 == historyCount) {
        self.gameHistoryLabel.alpha = 1.0f;
    } else {
        self.gameHistoryLabel.alpha = 0.5f;
    }
}

- (IBAction)changeGameType:(UISegmentedControl *)sender
{
    [self resetGame];
}

- (void)renderCards
{
    @throw [NSException
        exceptionWithName:NSInternalInconsistencyException
                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                 userInfo:nil];
}

- (void)updateUI
{
    DLog(@"");
    [self renderCards];

    self.scoreLabel.text = [NSString stringWithFormat:@"You have %lu points", (unsigned long)self.game.score];
    self.gameHistorySlider.hidden = ![self.gameHistoryCollection count];
}

- (void)resetGame
{
    self.gameHistoryCollection = nil;
    self.gameHistoryLabel.text = @"";
    self.gameHistorySlider.maximumValue = 0.2f;
    self.gameHistorySlider.value = 0.2f;

    self.game = nil;
    self.gameResult = nil;
    //    [self renderCards];
    self.flipsCount = 0;
    [self updateUI];
}

- (void)showGameEndAlert
{
    UIAlertView *theAlert = [[UIAlertView alloc] initWithTitle:@"Game Over"
                                                       message:@"There are no more matches available."
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil];
    [theAlert show];
}

#pragma mark -
#pragma mark Setters / Getters

- (CSGameResults *)gameResult
{
    if (!_gameResult) {
        _gameResult = [[CSGameResults alloc] initWithGameType:self.game.gameType];
    }
    return _gameResult;
}

- (NSMutableArray *)gameHistoryCollection
{
    if (!_gameHistoryCollection) {
        _gameHistoryCollection = [[NSMutableArray alloc] init];
    }

    return _gameHistoryCollection;
}

- (CSCardMatchingGame *)game
{
    @throw [NSException
        exceptionWithName:NSInternalInconsistencyException
                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                 userInfo:nil];

    return nil;
}

- (void)setCardButtons:(NSArray *)cardButtons
{
    @throw [NSException
        exceptionWithName:NSInternalInconsistencyException
                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                 userInfo:nil];
}

- (void)setFlipsCount:(NSUInteger)flipsCount
{
    _flipsCount = flipsCount;
    self.flipsLabel.text = [NSString stringWithFormat:@"Flips: %lu", (unsigned long)self.flipsCount];
}

#pragma mark -
#pragma mark Class Overrides

- (void)viewDidLoad
{
    [self resetGame];
    // make sure were are card matching combinations available
    if (![self.game areAnyMatchesAvailable]) {
        [self showGameEndAlert];
    }
}

@end
