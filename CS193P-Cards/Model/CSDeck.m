//
//  CSDeck.m
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-12-21.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import "CSDeck.h"

#pragma mark -
#pragma mark Class Extension

@interface CSDeck ()

@property (strong, nonatomic) NSMutableArray *cards;

@end

#pragma mark -
#pragma mark Class Implementation

@implementation CSDeck

@synthesize cards = _cards;

- (NSMutableArray *)cards
{
    if (!_cards) {
        _cards = [[NSMutableArray alloc] init];
    }

    return _cards;
}

-(void)addCard:(CSCard *)card atTop:(BOOL)atTop
{
    if (atTop) {
        [self.cards insertObject:card atIndex:0];
    }
    else {
        [self.cards addObject:card];
    }
}

-(CSCard *)drawRandomCard
{
    CSCard *randomCard = nil;

    if ([self.cards count]) {
        NSUInteger index = arc4random() % [self.cards count];
        randomCard = self.cards[index];
        [self.cards removeObjectAtIndex:index];
    }

    return randomCard;
}

@end
