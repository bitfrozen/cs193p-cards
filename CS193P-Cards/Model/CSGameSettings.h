//
//  CSGameSettings.h
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-10-01.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CSGameSettings : NSObject

+ (NSUInteger)getCardGameType;
+ (void)setCardGameType:(NSUInteger)index;

+ (NSUInteger)getMatchismoMatchingPoints;
+ (NSUInteger)getMatchismoMismatchPoints;
+ (NSUInteger)getMatchismoFlipPoints;
+ (void)setMatchismoMatchingPoints:(NSUInteger)points;
+ (void)setMatchismoMismatchPoints:(NSUInteger)points;
+ (void)setMatchismoFlipPoints:(NSUInteger)points;

+ (NSUInteger)getSetPointsForRightSet;
+ (NSUInteger)getSetPointsForWrongSet;
+ (void)setSetPointsForRightSet:(NSUInteger)points;
+ (void)setSetPointsForWrongSet:(NSUInteger)points;

+ (NSDictionary *)defaultParameters;
@end
