//
//  CSDeck.h
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-12-21.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSCard.h"

@interface CSDeck : NSObject

- (void)addCard:(CSCard *)card atTop:(BOOL)atTop;
- (CSCard *)drawRandomCard;

@end
