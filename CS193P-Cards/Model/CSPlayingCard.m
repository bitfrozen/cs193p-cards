//
//  CSPlayingCard.m
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-12-21.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import "CSPlayingCard.h"


#pragma mark -
#pragma mark Class Extension

@interface CSPlayingCard ()

- (NSUInteger)recursiveMatchingOfCard:(CSPlayingCard *)card withCards:(NSMutableArray *)cardsArray;

@end

#pragma mark -
#pragma mark Class Implementation

@implementation CSPlayingCard

@synthesize suit = _suit;
@synthesize rank = _rank;

- (NSString *)contents
{
    return [self.suit stringByAppendingString:[CSPlayingCard rankStrings][self.rank]];
}

+ (NSArray *)validSuits
{
    return @[@"♥", @"♦", @"♠", @"♣"];
}

+ (NSUInteger)maxRank
{
    return [[self rankStrings] count] - 1;
}

+ (NSArray *)rankStrings
{
    return @[@"?",@"A",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"J",@"Q",@"K"];
}

#pragma mark -
#pragma mark Setters/Getters

- (void)setSuit:(NSString *)suit
{
    if ([[CSPlayingCard validSuits] containsObject:suit]) {
        _suit = suit;
    }
}

- (NSString *)suit
{
    return _suit ? _suit : @"?";
}

- (void)setRank:(NSUInteger)rank
{
    if (rank <= [CSPlayingCard maxRank]) {
        _rank = rank;
    }
}

#pragma mark -
#pragma mark Overriding Super

- (NSUInteger)match:(NSArray *)otherCards
{
    NSUInteger score = 0;

    // Run match only if all input cards are Playing cards
    for (id card in otherCards) {
        if (![card isKindOfClass:[CSPlayingCard class]]) {
            return score;
        }
    }

    // Start recursive matching
    NSMutableArray *otherCardsMutable = [otherCards mutableCopy];
    score = [self recursiveMatchingOfCard:self withCards:otherCardsMutable];

    return score;
}

- (NSUInteger)recursiveMatchingOfCard:(CSPlayingCard *)card withCards:(NSMutableArray *)cardsArray
{
    DLog(@"Matching card %@", card);
    NSUInteger score = 0;
    // Matching procedure
    for (CSPlayingCard *tempCard in cardsArray) {
        if (card.rank == tempCard.rank) {
            score += 3;
            break;
        }
        if ([card.suit isEqualToString:tempCard.suit]) {
            score += 1;
        }
    }
    if ([cardsArray count] > 1) {
        // Splitting last element for recursion
        CSPlayingCard *newCard = [cardsArray lastObject];
        [cardsArray removeLastObject];
        return score + [self recursiveMatchingOfCard:newCard withCards:cardsArray];
    } else {
        return score;
    }
}

@end
