//
//  CSCard.h
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-12-21.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CSCard : NSObject

@property (strong, nonatomic) NSString *contents;
@property (nonatomic, getter = isFaceUp) BOOL faceUp;
@property (nonatomic, getter = isUnplayable) BOOL unplayable;

- (NSUInteger)match:(NSArray *)otherCards;

@end
