//
//  CSSetCardDeck.m
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-09-22.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import "CSSetCardDeck.h"
#import "CSSetCard.h"

@implementation CSSetCardDeck

- (id)init
{
    self = [super init];
    if (self) {
        for (NSUInteger shape = 0; shape <= [CSSetCard maxShape]; ++shape) {
            for (NSUInteger number = 0; number <= [CSSetCard maxNumber]; ++number) {
                for (NSUInteger color = 0; color <= [CSSetCard maxColor]; ++color) {
                    for (NSUInteger shade = 0; shade <= [CSSetCard maxShade]; ++shade) {
                        CSSetCard *card = [[CSSetCard alloc] init];
                        card.shape = shape;
                        card.number = number;
                        card.color = color;
                        card.shade = shade;
                        [self addCard:card atTop:NO];
                    }
                }
            }
        }
    }
    return self;
}

@end
