//
//  CSCardMatchingGame.h
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-12-21.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSGameMove.h"
#import "CSGameResults.h"

@class CSCard;
@class CSDeck;

@interface CSCardMatchingGame : NSObject

// designated initializer
- (id)initWithCardCount:(NSUInteger)count
              usingDeck:(CSDeck *)deck
               gameType:(CSGameType)gameType
             matchBonus:(NSUInteger)matchBonus
           mismatchCost:(NSUInteger)mismatchCost
               flipCost:(NSUInteger)flipCost;

- (BOOL)flipCardAtIndex:(NSUInteger)index;
- (CSCard *)cardAtIndex:(NSUInteger)index;
- (CSGameMove *)lastGameMove;
- (BOOL)areAnyMatchesAvailable;

@property(readonly, nonatomic) NSUInteger score;
@property(readonly, strong, nonatomic) NSMutableArray *gameMovesCollection;
@property(strong, nonatomic) NSArray *numberOfCardsPerGameType;
@property(nonatomic) CSGameType gameType;

@end
