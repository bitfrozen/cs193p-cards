//
//  CSGameResults.m
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-09-18.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import "CSGameResults.h"

@interface CSGameResults ()
@property(readwrite, nonatomic) NSDate *start;
@property(readwrite, nonatomic) NSDate *end;
@end

@implementation CSGameResults

@synthesize start = _start;
@synthesize end = _end;
@synthesize duration = _duration;
@synthesize gameType = _gameType;
@synthesize score = _score;

#define ALL_RESULTS_KEY @"GameResult_All"
#define START_KEY @"StartDate"
#define END_KEY @"EndDate"
#define SCORE_KEY @"Score"
#define GAME_TYPE_KEY @"GameType"

+ (NSArray *)allGameResults
{
    NSMutableArray *allGameResults = [[NSMutableArray alloc] init];

    for (id plist in [[[NSUserDefaults standardUserDefaults] dictionaryForKey:ALL_RESULTS_KEY] allValues]) {
        CSGameResults *result = [[CSGameResults alloc] initFromProperyList:plist];
        [allGameResults addObject:result];
    }

    return allGameResults;
}

+ (void)clearStoredGameResults
{
    DLog(@"Deleting all stored game results");
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:ALL_RESULTS_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)gameNameForType:(CSGameType)type
{
    switch (type) {
        case GameTypeTwoCardMatchingGame:
            return @"2CARD";
            break;
        case GameTypeThreeCardMatchingGame:
            return @"3CARD";
            break;
        case GameTypeSetMatchingGame:
            return @"SET";
            break;

        default:
            return @"INVALID";
            break;
    }
}

- (NSComparisonResult)compareScores:(CSGameResults *)result
{
    if (self.score > result.score) {
        return NSOrderedAscending;
    } else if (self.score < result.score) {
        return NSOrderedDescending;
    } else {
        return NSOrderedSame;
    }
}

- (NSComparisonResult)compareDates:(CSGameResults *)result
{
    return [self.end compare:result.end];
}

- (NSComparisonResult)compareDuration:(CSGameResults *)result
{
    if (self.duration > result.duration) {
        return NSOrderedDescending;
    } else if (self.duration < result.duration) {
        return NSOrderedAscending;
    } else {
        return NSOrderedSame;
    }
}

- (void)synchronize
{
    NSMutableDictionary *mutableGameResultsFromUserDefaults
        = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:ALL_RESULTS_KEY] mutableCopy];
    // If there are no game results stored, create empty dictionary
    if (!mutableGameResultsFromUserDefaults) {
        mutableGameResultsFromUserDefaults = [[NSMutableDictionary alloc] init];
    }
    mutableGameResultsFromUserDefaults[[self.start description]] = [self asPropertyList];
    [[NSUserDefaults standardUserDefaults] setObject:mutableGameResultsFromUserDefaults forKey:ALL_RESULTS_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (id)asPropertyList
{
    return @{ START_KEY : self.start, GAME_TYPE_KEY : @(self.gameType), END_KEY : self.end, SCORE_KEY : @(self.score) };
}

#pragma mark - Properties

- (NSTimeInterval)duration
{
    return [self.end timeIntervalSinceDate:self.start];
}

- (void)setScore:(int)score
{
    _score = score;
    self.end = [NSDate date];
    [self synchronize];
}

#pragma mark - Initializers

// convinience initialize
- (id)initFromProperyList:(id)plist
{
    self = [self init];
    if (self) {
        if ([plist isKindOfClass:[NSDictionary class]]) {
            NSDictionary *resultDictionary = (NSDictionary *)plist;
            _start = resultDictionary[START_KEY];
            _end = resultDictionary[END_KEY];
            _score = [resultDictionary[SCORE_KEY] intValue];
            _gameType = [resultDictionary[GAME_TYPE_KEY] integerValue];
            if (!_start || !_end) {
                self = nil;
            }
        }
    }

    return self;
}

// designated initializer
- (id)initWithGameType:(CSGameType)type
{
    self = [super init];
    if (self) {
        _start = [NSDate date];
        _gameType = type;
        _end = _start;
    }

    return self;
}

// default initializer
- (id)init
{
    self = [super init];
    if (self) {
        _start = [NSDate date];
        _gameType = GameTypeInvalid;
        _end = _start;
    }

    return self;
}

@end
