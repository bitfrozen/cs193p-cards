//
//  CSSetCard.h
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-09-20.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import "CSCard.h"

@interface CSSetCard : CSCard

@property(nonatomic) NSUInteger shape;
@property(nonatomic) NSUInteger color;
@property(nonatomic) NSUInteger shade;
@property(nonatomic) NSUInteger number;

+ (NSUInteger)maxShape;
+ (NSUInteger)maxShade;
+ (NSUInteger)maxNumber;
+ (NSUInteger)maxColor;

@end
