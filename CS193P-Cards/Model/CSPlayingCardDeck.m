//
//  CSPlayingCardDeck.m
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-12-21.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import "CSPlayingCardDeck.h"
#import "CSPlayingCard.h"

@implementation CSPlayingCardDeck

- (id)init
{
    self = [super init];
    if (self) {
        for (NSString *suit in [CSPlayingCard validSuits]) {
            for (NSUInteger rank = 1; rank <= [CSPlayingCard maxRank]; ++rank){
                CSPlayingCard *card = [[CSPlayingCard alloc] init];
                card.rank = rank;
                card.suit = suit;
                [self addCard:card atTop:NO];
            }
        }
    }
    
    return self;
}

@end
