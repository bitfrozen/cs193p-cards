//
//  CSGameResults.h
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-09-18.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GlobalTypes.h"

@interface CSGameResults : NSObject
// designated initializer
- (id)initWithGameType:(CSGameType)gameType;

+ (NSArray *)allGameResults; // of GameResults
+ (void)clearStoredGameResults;
+ (NSString *)gameNameForType:(CSGameType)type;

- (NSComparisonResult)compareScores:(CSGameResults *)result;
- (NSComparisonResult)compareDates:(CSGameResults *)result;
- (NSComparisonResult)compareDuration:(CSGameResults *)result;

@property(readonly, nonatomic) NSDate *start;
@property(readonly, nonatomic) NSDate *end;
@property(readonly, nonatomic) NSTimeInterval duration;
@property(nonatomic) CSGameType gameType;
@property(nonatomic) int score;

@end
