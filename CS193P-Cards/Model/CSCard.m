//
//  CSCard.m
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-12-21.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import "CSCard.h"

@implementation CSCard

@synthesize contents = _contents;
@synthesize faceUp = _faceUp;
@synthesize unplayable = _unplayable;

- (NSUInteger)match:(NSArray *)otherCards
{
    NSUInteger score = 0;
    for (CSCard *card in otherCards) {
        if ([card.contents isEqualToString:self.contents]) {
            score = 1;
        }
    }

    return score;
}

- (NSString *)description
{
    return self.contents;
}

@end
