//
//  CSGameMove.m
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-12-24.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import "CSGameMove.h"

@implementation CSGameMove

@synthesize moveType = _moveType;
@synthesize cardsInMove = _cardsInMove;
@synthesize moveScore = _moveScore;

- (id)init
{
    return [self initMove:MoveTypeFlip withCards:@[] withScore:0];
}

- (id)initMove:(CSMoveType)moveType withCards:(NSArray *)cards withScore:(NSInteger)score
{
    self = [super init];
    if (self) {
        _moveType = moveType;
        _cardsInMove = [cards mutableCopy];
        _moveScore = score;
    }

    return self;
}

-(void)addCard:(id)card
{
    DLog(@"Adding card to move: %@", card);
    if (!_cardsInMove) {
        _cardsInMove = [[NSMutableArray alloc] init];
    }
    [_cardsInMove addObject:card];
}

- (void)addCards:(NSArray *)cards
{
    DLog(@"Adding cards to move: %@", cards);
    if (!_cardsInMove) {
        _cardsInMove = [[NSMutableArray alloc] init];
    }
    for (id c in cards) {
        [_cardsInMove addObject:c];
    }
}

@end
