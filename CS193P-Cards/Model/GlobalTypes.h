//
//  GlobalTypes.h
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-12-29.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#ifndef CS193P_Cards_GlobalTypes_h
#define CS193P_Cards_GlobalTypes_h

typedef NS_ENUM(NSUInteger, CSGameType) { GameTypeInvalid, GameTypeTwoCardMatchingGame, GameTypeThreeCardMatchingGame, GameTypeSetMatchingGame };

#endif
