//
//  CSPlayingCardDeck.h
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-12-21.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import "CSDeck.h"

@interface CSPlayingCardDeck : CSDeck

@end
