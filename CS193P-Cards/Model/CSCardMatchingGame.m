//
//  CSCardMatchingGame.m
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-12-21.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import "CSCardMatchingGame.h"
#import "CSCard.h"
#import "CSDeck.h"

#pragma mark -
#pragma mark Class Extension

@interface CSCardMatchingGame ()

@property(readwrite, nonatomic) NSUInteger score;
@property(strong, nonatomic) NSMutableArray *cards; // of Card
@property(readwrite, strong, nonatomic) NSMutableArray *gameMovesCollection;
@property(nonatomic) NSUInteger matchBonus;
@property(nonatomic) NSUInteger mismatchCost;
@property(nonatomic) NSUInteger flipCost;

@end

#pragma mark -
#pragma mark Class Implemenation

@implementation CSCardMatchingGame

@synthesize score = _score;
@synthesize cards = _cards;
@synthesize gameMovesCollection = _gameMovesCollection;
@synthesize gameType = _gameType;
@synthesize matchBonus = _matchBonus;
@synthesize mismatchCost = _mismatchCost;
@synthesize flipCost = _flipCost;
@synthesize numberOfCardsPerGameType = _numberOfCardsPerGameType;

- (id)initWithCardCount:(NSUInteger)count
              usingDeck:(CSDeck *)deck
               gameType:(CSGameType)type
             matchBonus:(NSUInteger)matchBonus
           mismatchCost:(NSUInteger)mismatchCost
               flipCost:(NSUInteger)flipCost
{
    self = [super init];
    if (self) {
        for (NSUInteger i = 0; i < count; ++i) {
            CSCard *card = [deck drawRandomCard];
            if (card) {
                self.cards[i] = card;
            } else {
                self = nil;
                return self;
            }
        }
        _gameType = type;
        _matchBonus = matchBonus;
        _mismatchCost = mismatchCost;
        _flipCost = flipCost;
    }

    return self;
}

/**
 *  Method which performs actual flip inside our model.
 *
 *  @param index Index of the card, which is being fliped
 *
 *  @return Returns TRUE if there were changes inside model.
 */
- (BOOL)flipCardAtIndex:(NSUInteger)index
{
    CSCard *card = [self cardAtIndex:index];
    CSGameMove *thisMove = [[CSGameMove alloc] init];
    BOOL modelChanged = NO;

    if (card && !card.isUnplayable) {
        if (!card.isFaceUp) {

            // Go throu all cards and pick up cards which are faceUp and not "disabled"
            for (CSCard *otherCard in self.cards) {
                if (otherCard.isFaceUp && !otherCard.isUnplayable) {
                    // add these cards to move object
                    [thisMove addCard:otherCard];
                }
            }

            // check if number of picked cards is enough to perform a match (depending on game type)
            // (adding +1 to account for actual card we are flipping,
            // since we are adding it move object only at the end of method)
            if ([thisMove.cardsInMove count] + 1
                >= [(NSNumber *)self.numberOfCardsPerGameType[self.gameType] integerValue]) { // Fucking ugly =(
                DLog(@"Enought cards - perform matching");
                NSInteger matchScore = [card match:thisMove.cardsInMove];
                if (matchScore) {
                    card.unplayable = YES;
                    for (CSCard *o in thisMove.cardsInMove) {
                        o.unplayable = YES;
                    }
                    self.score += matchScore * self.matchBonus;
                    thisMove.moveScore = matchScore * self.matchBonus;
                    thisMove.moveType = MoveTypeMatch;
                } else {
                    for (CSCard *o in thisMove.cardsInMove) {
                        o.faceUp = NO;
                    }
                    self.score -= self.mismatchCost;
                    thisMove.moveScore = self.mismatchCost;
                    thisMove.moveType = MoveTypeMismatch;
                }
            }

            // finally add actual card we just flipped to move object
            [thisMove addCard:card];

            self.score -= self.flipCost;
            [self.gameMovesCollection addObject:thisMove];
            modelChanged = YES;
        }
        card.faceUp = !card.isFaceUp;
    }

    return modelChanged;
}

- (CSCard *)cardAtIndex:(NSUInteger)index
{
    return (index < [self.cards count]) ? self.cards[index] : nil;
}

- (CSGameMove *)lastGameMove
{
    return (CSGameMove *)[self.gameMovesCollection lastObject];
}

- (BOOL)areAnyMatchesAvailable
{
    DLog(@"Looking for matches...");
    // If there are more then 2 game types
    // change to switch
    if (self.gameType == GameTypeTwoCardMatchingGame) {
        for (NSUInteger i = 0; i < [self.cards count] - 1; ++i) {
            CSCard *card = [self cardAtIndex:i];
            if (![card isUnplayable]) {
                for (NSUInteger j = i + 1; j < [self.cards count]; ++j) {
                    if (![[self cardAtIndex:j] isUnplayable] && [[self cardAtIndex:j] match:@[ card ]]) {
                        DLog(@"Matching cards %@ and %@", card.contents, [self cardAtIndex:j].contents);
                        return YES;
                    }
                }
            }
        }
        return NO;
    } else if (self.gameType == GameTypeThreeCardMatchingGame || self.gameType == GameTypeSetMatchingGame) {
        for (NSUInteger i = 0; i < [self.cards count] - 2; ++i) {
            CSCard *first_card = [self cardAtIndex:i];
            if (![first_card isUnplayable]) {
                for (NSUInteger j = i + 1; j < [self.cards count] - 1; ++j) {
                    CSCard *second_card = [self cardAtIndex:j];
                    if (![second_card isUnplayable]) {
                        for (NSUInteger z = j + 1; z < [self.cards count]; ++z) {
                            if (![[self cardAtIndex:z] isUnplayable]
                                    && [[self cardAtIndex:z] match:@[ first_card, second_card ]]) {
                                DLog(@"Matching cards %@, %@ and %@", first_card.contents, second_card.contents,
                                     [self cardAtIndex:z].contents);
                                return YES;
                            }
                        }
                    }
                }
            }
        }
        return NO;
    } else {
        return NO;
    }
}

#pragma mark -
#pragma mark Setters / Getters

- (NSMutableArray *)gameMovesCollection
{
    if (!_gameMovesCollection) {
        _gameMovesCollection = [[NSMutableArray alloc] init];
    }

    return _gameMovesCollection;
}

- (NSMutableArray *)cards
{
    if (!_cards) {
        _cards = [[NSMutableArray alloc] init];
    }

    return _cards;
}

- (NSArray *)numberOfCardsPerGameType
{
    if (!_numberOfCardsPerGameType) {
        _numberOfCardsPerGameType = @[ @0, @2, @3, @3 ];
    }

    return _numberOfCardsPerGameType;
}

@end
