//
//  CSGameSettings.m
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-10-01.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import "CSGameSettings.h"

@implementation CSGameSettings

#define CARD_GAME_TYPE @"CardGameType"
#define MATCHISMO_MATCH_POINTS @"MatchismoMatchPoints"
#define MATCHISMO_MISMATCH_POINTS @"MatchismoMismatchPoints"
#define MATCHISMO_FLIP_POINTS @"MatchismoFlipPoints"
#define SET_RIGHT_SET_POINTS @"RightSetPoints"
#define SET_WRONG_SET_POINTS @"WrongSetPoints"

+ (NSUInteger)getCardGameType
{
    return (NSUInteger)[[NSUserDefaults standardUserDefaults] integerForKey : CARD_GAME_TYPE];
}

+ (void)setCardGameType:(NSUInteger)index
{
    [[NSUserDefaults standardUserDefaults] setInteger:(NSInteger)index forKey:CARD_GAME_TYPE];
}

+ (NSUInteger)getMatchismoMatchingPoints
{
    return (NSUInteger)[[NSUserDefaults standardUserDefaults] integerForKey : MATCHISMO_MATCH_POINTS];
}

+ (NSUInteger)getMatchismoMismatchPoints
{
    return (NSUInteger)[[NSUserDefaults standardUserDefaults] integerForKey : MATCHISMO_MISMATCH_POINTS];
}

+ (NSUInteger)getMatchismoFlipPoints
{
    return (NSUInteger)[[NSUserDefaults standardUserDefaults] integerForKey : MATCHISMO_FLIP_POINTS];
}

+ (void)setMatchismoMatchingPoints:(NSUInteger)points
{
    [[NSUserDefaults standardUserDefaults] setInteger:(NSInteger)points forKey:MATCHISMO_MATCH_POINTS];
}

+ (void)setMatchismoMismatchPoints:(NSUInteger)points
{
    [[NSUserDefaults standardUserDefaults] setInteger:(NSInteger)points forKey:MATCHISMO_MISMATCH_POINTS];
}

+ (void)setMatchismoFlipPoints:(NSUInteger)points
{
    [[NSUserDefaults standardUserDefaults] setInteger:(NSInteger)points forKey:MATCHISMO_FLIP_POINTS];
}

+ (NSUInteger)getSetPointsForRightSet
{
    return (NSUInteger)[[NSUserDefaults standardUserDefaults] integerForKey : SET_RIGHT_SET_POINTS];
}

+ (NSUInteger)getSetPointsForWrongSet
{
    return (NSUInteger)[[NSUserDefaults standardUserDefaults] integerForKey : SET_WRONG_SET_POINTS];
}

+ (void)setSetPointsForRightSet:(NSUInteger)points
{
    [[NSUserDefaults standardUserDefaults] setInteger:(NSInteger)points forKey:SET_RIGHT_SET_POINTS];
}

+ (void)setSetPointsForWrongSet:(NSUInteger)points
{
    [[NSUserDefaults standardUserDefaults] setInteger:(NSInteger)points forKey:SET_WRONG_SET_POINTS];
}

+ (NSDictionary *)defaultParameters
{
    return @{
        CARD_GAME_TYPE : @(2),
        MATCHISMO_MATCH_POINTS : @(4),
        MATCHISMO_MISMATCH_POINTS : @(2),
        MATCHISMO_FLIP_POINTS : @(1),
        SET_RIGHT_SET_POINTS : @(6),
        SET_WRONG_SET_POINTS : @(8)
    };
}
@end
