//
//  CSGameMove.h
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-12-24.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, CSMoveType)
{
    MoveTypeFlip,
    MoveTypeMatch,
    MoveTypeMismatch
};

@interface CSGameMove : NSObject

- (id)init;
- (id)initMove:(CSMoveType)moveType
     withCards:(NSArray *)cards
     withScore:(NSInteger)score;
- (void)addCard:(id)card;
- (void)addCards:(NSArray *)cards;

@property (nonatomic) CSMoveType moveType;
@property (readonly, strong, nonatomic) NSMutableArray *cardsInMove;
@property (nonatomic) NSInteger moveScore;

@end
