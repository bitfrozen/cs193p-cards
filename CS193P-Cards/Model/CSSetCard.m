//
//  CSSetCard.m
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-09-20.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import "CSSetCard.h"

@implementation CSSetCard

@synthesize shape = _shape;
@synthesize color = _color;
@synthesize shade = _shade;
@synthesize number = _number;

- (NSUInteger)match:(NSArray *)otherCards
{
    NSUInteger score = 0;

    // Running match only if there are 3 cards
    if ([otherCards count] == 2) {
        for (id card in otherCards) {
            // check that cards would be of type SetCards
            if (![card isKindOfClass:[self class]]) {
                return score;
            }
        }

        // Geting other cards
        CSSetCard *card1 = otherCards[0];
        CSSetCard *card2 = otherCards[1];

        /*
         *    Checking each category if it's 2 of ... 1 of ...
         *    If at least one category is, these cards aren't set
         */
        // Check shape
        if ((self.shape == card1.shape && self.shape != card2.shape)
            || (self.shape == card2.shape && self.shape != card1.shape)
            || (card1.shape == card2.shape && self.shape != card1.shape)) {

            return score;
        }
        // Check number
        if ((self.number == card1.number && self.number != card2.number)
            || (self.number == card2.number && self.number != card1.number)
            || (card1.number == card2.number && self.number != card1.number)) {

            return score;
        }
        // Check color
        if ((self.color == card1.color && self.color != card2.color)
            || (self.color == card2.color && self.color != card1.color)
            || (card1.color == card2.color && self.color != card1.color)) {

            return score;
        }
        // Check shade
        if ((self.shade == card1.shade && self.shade != card2.shade)
            || (self.shade == card2.shade && self.shade != card1.shade)
            || (card1.shade == card2.shade && self.shade != card1.shade)) {

            return score;
        }

        score = 3;
    }

    return score;
}

- (NSString *)contents
{
    return [NSString stringWithFormat:@"shape%lu&number%lu&color%lu&shade%lu", (unsigned long)self.shape,
                                      (unsigned long)self.number, (unsigned long)self.color, (unsigned long)self.shade];
}

#pragma mark -
#pragma mark Setters

- (void)setColor:(NSUInteger)color
{
    if (color <= [[self class] maxColor]) {
        _color = color;
    }
}

- (void)setShade:(NSUInteger)shade
{
    if (shade <= [[self class] maxShade]) {
        _shade = shade;
    }
}

- (void)setShape:(NSUInteger)shape
{
    if (shape <= [[self class] maxShape]) {
        _shape = shape;
    }
}

- (void)setNumber:(NSUInteger)number
{
    if (number <= [[self class] maxNumber]) {
        _number = number;
    }
}

#pragma mark -
#pragma mark Class methods

+ (NSUInteger)maxColor
{
    return 2;
}

+ (NSUInteger)maxNumber
{
    return 2;
}

+ (NSUInteger)maxShade
{
    return 2;
}

+ (NSUInteger)maxShape
{
    return 2;
}

@end
