//
//  CSGameSettingsViewController.m
//  CS193P-Cards
//
//  Created by Antanas Domarkas on 2013-10-01.
//  Copyright (c) 2013 Curonian Software. All rights reserved.
//

#import "CSGameSettingsViewController.h"
#import "CSGameResults.h"
#import "CSGameSettings.h"

@interface CSGameSettingsViewController ()
@property(strong, nonatomic) IBOutlet UIScrollView *innerScrollView;
@property(weak, nonatomic) IBOutlet UISegmentedControl *cardGameType;
//
// Outlets for setting sliders
//
@property(weak, nonatomic) IBOutlet UISlider *pointsForMatchSlider;
@property(weak, nonatomic) IBOutlet UISlider *pointsForMismatchSlider;
@property(weak, nonatomic) IBOutlet UISlider *pointsForFlipSlider;
@property(weak, nonatomic) IBOutlet UISlider *pointsForRightSetSlider;
@property(weak, nonatomic) IBOutlet UISlider *pointsForWrongSetSlider;
//
// Outlets for settings slider labels
//
@property(weak, nonatomic) IBOutlet UILabel *pointsForMatchSliderLabel;
@property(weak, nonatomic) IBOutlet UILabel *pointsForMismatchSliderLabel;
@property(weak, nonatomic) IBOutlet UILabel *pointsForFlipSliderLabel;
@property(weak, nonatomic) IBOutlet UILabel *pointsForRightSetSliderLabel;
@property(weak, nonatomic) IBOutlet UILabel *pointsForWrongSetSliderLabel;
//
// Actions for settings sliders values changed
//
- (IBAction)pointsForMatchChanged:(UISlider *)sender;
- (IBAction)pointsForMismatchChanged:(UISlider *)sender;
- (IBAction)pointsForFlipChanged:(UISlider *)sender;
- (IBAction)pointsForRightSetChanged:(UISlider *)sender;
- (IBAction)pointsForWrongSetChanged:(UISlider *)sender;

- (IBAction)resetGameScoresTouch;
- (IBAction)changeCardGameType:(UISegmentedControl *)sender;

@end

@implementation CSGameSettingsViewController

@synthesize pointsForMatchSlider = _pointsForMatchSlider;
@synthesize pointsForMismatchSlider = _pointsForMismatchSlider;
@synthesize pointsForFlipSlider = _pointsForFlipSlider;
@synthesize pointsForRightSetSlider = _pointsForRightSetSlider;
@synthesize pointsForWrongSetSlider = _pointsForWrongSetSlider;
@synthesize pointsForMatchSliderLabel = _pointsForMatchSliderLabel;
@synthesize pointsForMismatchSliderLabel = _pointsForMismatchSliderLabel;
@synthesize pointsForFlipSliderLabel = _pointsForFlipSliderLabel;
@synthesize pointsForRightSetSliderLabel = _pointsForRightSetSliderLabel;
@synthesize pointsForWrongSetSliderLabel = _pointsForWrongSetSliderLabel;

- (IBAction)resetGameScoresTouch
{
    [CSGameResults clearStoredGameResults];
}

- (IBAction)changeCardGameType:(UISegmentedControl *)sender
{
    [CSGameSettings setCardGameType:sender.selectedSegmentIndex];
}

// Set up values for sliders and labels
- (void)setupSlider:(UISlider *)slider andLabel:(UILabel *)label withValue:(NSUInteger)value
{
    slider.continuous = YES;
    slider.minimumValue = 0.0f;
    slider.maximumValue = 10.0f;
    slider.value = (float)value;
    [self setValue:value forLabel:label];
}

- (void)setValue:(NSUInteger)value forLabel:(UILabel *)label
{
    label.text = [NSString stringWithFormat:@"%lu", (unsigned long)value];
}

- (IBAction)pointsForMatchChanged:(UISlider *)sender
{
    [CSGameSettings setMatchismoMatchingPoints:(NSUInteger)(sender.value + 0.5f)];
    [self setValue:(NSUInteger)(sender.value + 0.5f)forLabel:self.pointsForMatchSliderLabel];
}

- (IBAction)pointsForMismatchChanged:(UISlider *)sender
{
    [CSGameSettings setMatchismoMismatchPoints:(NSUInteger)(sender.value + 0.5f)];
    [self setValue:(NSUInteger)(sender.value + 0.5f)forLabel:self.pointsForMismatchSliderLabel];
}

- (IBAction)pointsForFlipChanged:(UISlider *)sender
{
    [CSGameSettings setMatchismoFlipPoints:(NSUInteger)(sender.value + 0.5f)];
    [self setValue:(NSUInteger)(sender.value + 0.5f)forLabel:self.pointsForFlipSliderLabel];
}

- (IBAction)pointsForRightSetChanged:(UISlider *)sender
{
    [CSGameSettings setSetPointsForRightSet:(NSUInteger)(sender.value + 0.5f)];
    [self setValue:(NSUInteger)(sender.value + 0.5f)forLabel:self.pointsForRightSetSliderLabel];
}

- (IBAction)pointsForWrongSetChanged:(UISlider *)sender
{
    [CSGameSettings setSetPointsForWrongSet:(NSUInteger)(sender.value + 0.5f)];
    [self setValue:(NSUInteger)(sender.value + 0.5f)forLabel:self.pointsForWrongSetSliderLabel];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Enabling settings view scrolling
    self.innerScrollView.scrollEnabled = YES;
    self.innerScrollView.contentSize = CGSizeMake(320.0f, 580.0f);
    // setup card game type based on user preference
    self.cardGameType.selectedSegmentIndex = [CSGameSettings getCardGameType];

    // setup sliders based on user preference file
    [self setupSlider:self.pointsForMatchSlider
             andLabel:self.pointsForMatchSliderLabel
            withValue:[CSGameSettings getMatchismoMatchingPoints]];
    [self setupSlider:self.pointsForMismatchSlider
             andLabel:self.pointsForMismatchSliderLabel
            withValue:[CSGameSettings getMatchismoMismatchPoints]];
    [self setupSlider:self.pointsForFlipSlider
             andLabel:self.pointsForFlipSliderLabel
            withValue:[CSGameSettings getMatchismoFlipPoints]];
    [self setupSlider:self.pointsForRightSetSlider
             andLabel:self.pointsForRightSetSliderLabel
            withValue:[CSGameSettings getSetPointsForRightSet]];
    [self setupSlider:self.pointsForWrongSetSlider
             andLabel:self.pointsForWrongSetSliderLabel
            withValue:[CSGameSettings getSetPointsForWrongSet]];
}

- (void)viewWillDisappear:(BOOL)__unused animated
{
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
